# budgie-desktop-git

A familiar, modern desktop environment - latest git

https://github.com/BuddiesOfBudgie/budgie-desktop

<br>

How to clone this repository:
```
git clone https://gitlab.com/rebornos-team/rebornos-packages/budgie/budgie-desktop-git.git
```

